export interface ICurso {
	nombre: string;
	imagen: string;
  fecha:string;
  instructor:string;
}
