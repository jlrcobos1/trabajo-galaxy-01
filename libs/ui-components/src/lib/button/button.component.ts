import { Component, HostBinding, Input, OnInit } from '@angular/core';
import { ButtonColor, ButtonSize } from './button.types';

@Component({
  selector: 'exa-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss']
})
export class ButtonComponent implements OnInit {
  @Input() color: ButtonColor = 'primary';
  @Input() size: ButtonSize = '';
  @HostBinding('class')
  get className(): string {
    return `btn btn-${this.color} ${this.size}`;
  }
  constructor() { }

  ngOnInit(): void {
  }

}
