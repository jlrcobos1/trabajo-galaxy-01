import { Component, OnInit } from '@angular/core';
import { ICurso } from '../../interfaces/ICurso';

@Component({
  selector: 'app-cursos',
  templateUrl: './cursos.component.html',
  styleUrls: ['./cursos.component.scss']
})
export class CursosComponent implements OnInit {
  cursos: ICurso[]=[
    {nombre:"Angular HTTP",imagen:"assets/img/angular.png",fecha: Date(),instructor:"Julio ortiz campoz"},
    {nombre:"Automatización de pruebas en aplicaciones web con selenium",imagen:"assets/img/selenium.jpg",fecha: Date(),instructor:"Carmen salvador lopez"},
    {nombre:"Aplicaciones web con spring security",imagen:"assets/img/springsec.jpg",fecha: Date(),instructor:"Julio sandoval urbina"},
    {nombre:"Componentes de navegación en android",imagen:"assets/img/android.jpg",fecha: Date(),instructor:"Julio sandoval urbina"},
    {nombre:"Cultura y prácticas DevOps",imagen:"assets/img/devops.jpg",fecha: Date(),instructor:"Julio ortiz campoz"},
    {nombre:"Optimizando operaciones usando tablas particionadas",imagen:"assets/img/sql.png",fecha: Date(),instructor:"Carmen salvador lopez"},
    {nombre:"Angular interceptores",imagen:"assets/img/interceptores.png",fecha: Date(),instructor:"Julio sandoval urbina"},
    {nombre:"Integridad de datos con contraints y triggers",imagen:"assets/img/triggers.jpg",fecha: Date(),instructor:"Julio ortiz campoz"}
  ];
  constructor() { }

  ngOnInit(): void {
  }

}
