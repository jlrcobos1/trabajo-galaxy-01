export type ButtonColor =
  'primary' |
  'secondary' |
  'danger' |
  'success';
  export type ButtonSize =
   '' |
  'btn-sm' |
  'btn-lg' ;
